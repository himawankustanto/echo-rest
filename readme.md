# Echo Rest

### Setup

```
$ export GO111MODULE=on
```

### Required Variables
Env variables that need to be set are:
- `ENVIRONMENT`
- `API_PORT`
- `DB_USERNAME`
- `DB_PASSWORD`
- `DB_HOST`
- `DB_PORT`
- `DB_NAME`
- `DB_CHARSET`
- `DB_PARSETIME`
- `DB_LOC`

example in linux:
```
$ export ENVIRONMENT=development
$ export API_PORT=10000
$ export DB_USERNAME=root
$ export DB_PASSWORD=password
$ export DB_HOST=127.0.0.1
$ export DB_PORT=3306
$ export DB_NAME=test
$ export DB_CHARSET=utf8
$ export DB_PARSETIME=true
$ export DB_LOC=Local
```

### Build Swagger Documentation
Download Swag for Go by using:
```
$ go get -u github.com/swaggo/swag/cmd/swag
```
Run the Swag in your Go project root folder which contains main.go file
```
$ swag init
```
Note for development: If you are done with documentation, please execute this syntax

You can see the result on [http://localhost/swagger/index.html](http://localhost/swagger/index.html)

### Build
```
$ cp config_example.json config.json
$ go build main.go
```

### Run
```
$ ./main
```

### Database migrations

All migrations all kept in the `/db/migrations` dir.

Steps to create bash script to provide migration
- Migration Up Example
```
$ migrate -database "mysql://root:@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local" -path db/migrations up
```
- Migration Down Example
```
$ migrate -database "mysql://root:@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local" -path db/migrations down 1
```
- Migration Force Example
```
$ migrate -database "mysql://root:@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=True&loc=Local" -path db/migrations force 1
```
Refer to the [README](https://gitlab.smartfren.com/himawan/echo-rest/tree/master/db) in `db` for how to use.
