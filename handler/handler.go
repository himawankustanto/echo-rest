package handler

import (
	"github.com/jinzhu/gorm"
)

type DbPointer struct {
	Db *gorm.DB
}
