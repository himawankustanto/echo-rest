package config

import (
	"fmt"
	"github.com/jinzhu/gorm"
	"os"
	"strconv"

	//
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
)

// CheckRequiredEnv comment
func CheckRequiredEnv() error {
	if GetEnv("API_PORT") == "" {
		return fmt.Errorf("API_PORT not defined")
	}

	if GetEnv("ENVIRONMENT") == "" {
		return fmt.Errorf("ENVIRONMENT not defined")
	}

	if GetEnv("DB_HOST") == "" {
		return fmt.Errorf("DB_HOST not defined")
	}

	if GetEnv("DB_PORT") == "" {
		return fmt.Errorf("DB_PORT not defined")
	}

	if GetEnv("DB_NAME") == "" {
		return fmt.Errorf("DB_NAME not defined")
	}

	if GetEnv("DB_USERNAME") == "" {
		return fmt.Errorf("DB_USERNAME not defined")
	}

	//if GetEnv("DB_PASSWORD") == "" {
	//	return fmt.Errorf("DB_PASSWORD not defined")
	//}

	if GetEnv("DB_CHARSET") == "" {
		return fmt.Errorf("DB_CHARSET not defined")
	}

	if GetEnv("DB_PARSETIME") == "" {
		return fmt.Errorf("DB_PARSETIME not defined")
	}

	if GetEnv("DB_LOC") == "" {
		return fmt.Errorf("DB_LOC not defined")
	}

	return nil
}

// GetJSON comment
func GetJSON(key string) string {
	viper.SetConfigType("json")
	viper.AddConfigPath(".")
	viper.SetConfigName("config")
	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to read config")
	}

	value := viper.GetString(key)

	return value
}

// GetEnv comment
func GetEnv(key string) string {
	value := os.Getenv(key)
	return value
}

// DBInit comment
func DBInit() *gorm.DB {
	connectionString := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=%v&parseTime=%v&loc=%v",
		GetEnv("DB_USERNAME"),
		GetEnv("DB_PASSWORD"),
		GetEnv("DB_HOST"),
		GetEnv("DB_PORT"),
		GetEnv("DB_NAME"),
		GetEnv("DB_CHARSET"),
		GetEnv("DB_PARSETIME"),
		GetEnv("DB_LOC"),
	)

	db, err := gorm.Open("mysql", connectionString)
	if err != nil {
		fmt.Println(err.Error())
		panic("failed to connect to database")
	}
	dbLog, errDbLog := strconv.ParseBool(GetJSON("database.log"))
	if errDbLog != nil {
		fmt.Println(errDbLog.Error())
		panic("failed to parse boo l db_log")
	}
	db.LogMode(dbLog)
	return db
}
