package handler

import (
	"echo-rest/helper"
	"echo-rest/structs/model"
	"echo-rest/structs/requests"
	"echo-rest/structs/responses"
	"github.com/labstack/echo/v4"
	"net/http"
	"strconv"
)

// GetUsers godoc
// @Summary Get Users List
// @Accept json
// @Produce json
// @Tags Users
// @Success 200 {object} helper.Responses{data=[]responses.UserResponse}
// @Router /v1/users [get]
func (dbp *DbPointer) GetUsers(c echo.Context) error {
	var dataDb []model.User
	var dataList []responses.UserResponse

	err := dbp.Db.Find(&dataDb).Error
	if err != nil && err.Error() != model.RecordNotFound {
		return helper.CustomResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	for _, value := range dataDb {

		dataList = append(dataList, responses.UserResponse{
			UserId:    strconv.Itoa(value.UserId),
			Email:     value.Email,
			FirstName: value.FirstName,
			LastName:  value.LastName,
		})
	}

	return helper.CustomResponse(c, http.StatusOK, http.StatusText(http.StatusOK), &dataList)
}

// CreateUsers godoc
// @Summary Create Users
// @Accept json
// @Produce json
// @Tags Users
// @Param request body requests.CreateUserRequest true "Payload"
// @Success 201 {object} helper.Responses{data=responses.CreateUserResponse}
// @Router /v1/users [put]
func (dbp *DbPointer) CreateUsers(c echo.Context) error {
	var request requests.CreateUserRequest

	if err := c.Bind(&request); err != nil {
		errMessage := helper.ErrorValidationMessage(err)
		return helper.CustomResponse(c, http.StatusUnprocessableEntity, errMessage, nil)
	}

	if err := c.Validate(&request); err != nil {
		errMessage := helper.ErrorValidationMessage(err)
		return helper.CustomResponse(c, http.StatusBadRequest, errMessage, nil)
	}

	tx := dbp.Db.Begin()
	if err := tx.Error; err != nil {
		return helper.CustomResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	insert := model.User{
		Email:     request.Email,
		FirstName: request.FirstName,
		LastName:  request.LastName,
		Password:  request.Password,
	}

	if err := tx.Create(&insert).Error; err != nil {
		tx.Rollback()
		return helper.CustomResponse(c, http.StatusInternalServerError, err.Error(), nil)
	}

	userId := insert.GetUserId()

	responseParams := responses.CreateUserResponse{UserId: strconv.Itoa(userId)}
	tx.Commit()
	return helper.CustomResponse(c, http.StatusCreated, "Success", &responseParams)
}
