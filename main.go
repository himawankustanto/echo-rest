package main

import (
	"echo-rest/config"
	"echo-rest/docs"
	"echo-rest/handler"
	"echo-rest/helper"
	"echo-rest/structs/model"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/swaggo/echo-swagger"
	"net/http"
)

// @contact.name API Support
// @contact.email himawan2717@gmail.com
func main() {
	err := config.CheckRequiredEnv()
	if err != nil {
		panic(err)
	}

	docs.SwaggerInfo.Title = "Echo Rest"
	docs.SwaggerInfo.Description = "This is a test for Echo Rest."
	docs.SwaggerInfo.Version = config.GetJSON("app.version")
	docs.SwaggerInfo.Host = config.GetJSON("app.host")
	docs.SwaggerInfo.BasePath = "/"
	docs.SwaggerInfo.Schemes = []string{"http", "https"}

	db := &handler.DbPointer{Db: config.DBInit()}

	e := echo.New()
	e.Validator = &model.CustomValidator{Validator: validator.New()}
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.GET("/ping", Ping)
	e.GET("/swagger/*", echoSwagger.WrapHandler)

	api := e.Group("/v1")

	users := api.Group("/users")
	users.GET("", db.GetUsers)
	users.PUT("", db.CreateUsers)

	e.Logger.Fatal(e.Start(":" + config.GetEnv("API_PORT")))
}

// Ping godoc
// @Summary Ping
// @Accept json
// @Produce json
// @Tags [General] Ping
// @Success 200 {object} helper.Responses{message}
// @Router /ping [get]
func Ping(c echo.Context) error {
	return helper.CustomResponse(c, http.StatusOK, "pong", nil)
}
