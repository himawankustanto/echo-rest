BEGIN;
CREATE TABLE IF NOT EXISTS user
(
    user_id int auto_increment,
    email varchar(128) not null,
    first_name varchar(128) not null,
    last_name varchar(128) not null,
    password varchar(512) not null,
    created_at timestamp not null default CURRENT_TIMESTAMP,
    updated_at timestamp not null default CURRENT_TIMESTAMP,
    deleted_at datetime null,
    constraint user_pk
        primary key (user_id)
);
COMMIT;

