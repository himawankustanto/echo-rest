package model

import "time"

type User struct {
	UserId    int `gorm:"primary_key"`
	Email     string
	FirstName string
	LastName  string
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt *time.Time `sql:"index"`
}

func (User) TableName() string {
	return "user"
}

func (db User) GetUserId() int {
	return db.UserId
}
