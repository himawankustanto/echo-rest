# DB Migrations

## Installation
### Download pre-built binary (Windows, MacOS, or Linux)
[Release Downloads](https://github.com/golang-migrate/migrate/releases)
### MacOS
```bash
$ brew install golang-migrate
```
### Windows
Using [scoop](https://scoop.sh/)
```bash
$ scoop install migrate
```
### Linux (*.deb package)
```bash
$ curl -L https://packagecloud.io/golang-migrate/migrate/gpgkey | apt-key add -
$ echo "deb https://packagecloud.io/golang-migrate/migrate/ubuntu/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/migrate.list
$ apt-get update
$ apt-get install -y migrate
```
## Usage

```bash
$ migrate -help
Usage: migrate OPTIONS COMMAND [arg...]
       migrate [ -version | -help ]

Options:
  -source          Location of the migrations (driver://url)
  -path            Shorthand for -source=file://path
  -database        Run migrations against this database (driver://url)
  -prefetch N      Number of migrations to load in advance before executing (default 10)
  -lock-timeout N  Allow N seconds to acquire database lock (default 15)
  -verbose         Print verbose logging
  -version         Print version
  -help            Print usage

Commands:
  create [-ext E] [-dir D] [-seq] [-digits N] [-format] NAME
               Create a set of timestamped up/down migrations titled NAME, in directory D with extension E.
               Use -seq option to generate sequential up/down migrations with N digits.
               Use -format option to specify a Go time format string.
  goto V       Migrate to version V
  up [N]       Apply all or N up migrations
  down [N]     Apply all or N down migrations
  drop         Drop everything inside database
  force V      Set version V but don't run migration (ignores dirty state)
  version      Print current migration version
```

So let's say you want to run the first two migrations

```bash
$ migrate -source file://path/to/migrations -database postgres://localhost:5432/database up 2
```

If your migrations are hosted on github

```bash
$ migrate -source github://mattes:personal-access-token@mattes/migrate_test \
    -database postgres://localhost:5432/database down 2
```

The CLI will gracefully stop at a safe point when SIGINT (ctrl+c) is received.
Send SIGKILL for immediate halt.

Migrations always comes in two, up and down. `Up` is the change to be made to the database, e.g. `ALTER
TABLE client ADD COLUMN IF NOT EXISTS age SMALLINT NULL`. `Down` is how to _undo_ the change made in `Up`, so the `Down` migration
would be `ALTER TABLE client DROP COLUMN IF EXISTS age`. We're following the convention of the tool we are using, `migrate`.

## WARNING
### _Do not_ forget to use `IF` in your migrations when possible!!
```
ALTER TABLE client ADD COLUMN _IF NOT EXISTS_ age SMALLINT NULL`
```

### _Do not_ forget to wrap your migrations in a transaction!!
```
BEGIN;

some sql command 1;
...
some sql command n; 

COMMIT;
```

Steps to making a migration:
1. Make a migration with the command below. Two files will be generated: (1) 00000_some_migration.up.sql, (2) 00000_some_migration.down.sql
2. Write the up and down migrations within the respective files.
3. Run the migration with the command below. You can use the script, `migratup`, `migratedown`, and `migrateforce`.

Be careful of dirty states which happen when a migrations fails. `migrate` will not allow another migration if the state of the database is dirty. You will need to force
a version by running `migrate -database ${POSTGRESQL_URL} -path db/migrations force VERSION` to enable it again.


### Making migrations:
`migrate create -ext sql -dir db/migrations -seq tabel_action_column_names`

naming convention for migrations `TABLENAME_ACTION_COLUMN1-COLUMN2-COLUMN3`

### Migration command:
`migrate -database ${POSTGRESQL_URL} -path db/migrations up`

needs to have database URL as the above variable, so need:
`export POSTGRESQL_URL='postgres://dev_cbs:tUZwWN4a@127.0.0.1:5432/dev_cbs'`
