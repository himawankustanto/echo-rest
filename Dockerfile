FROM golang:1.13-alpine as build
RUN apk update && apk add --no-cache git
RUN go get -u github.com/swaggo/swag/cmd/swag
RUN mkdir /app
ADD . /app/
WORKDIR /app
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux swag init
RUN CGO_ENABLED=0 GOOS=linux go build -o main .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=0 /app .
CMD ["./main"]