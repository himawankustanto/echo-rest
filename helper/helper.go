package helper

import (
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo/v4"
	"strings"
)

type Responses struct {
	Data    interface{} `json:"data"`
	Message interface{} `json:"message"`
}

// CustomResponse comment
func CustomResponse(c echo.Context, code int, message interface{}, data interface{}) error {
	return c.JSON(code, &Responses{
		Data:    data,
		Message: message,
	})
}

// ErrorValidationMessage comment
func ErrorValidationMessage(err error) string {
	var data []string

	if validationError, ok := err.(validator.ValidationErrors); ok {
		for _, fieldErr := range validationError {
			switch fieldErr.Tag() {
			case "required":
				data = append(data, fmt.Sprintf("%s is required", fieldErr.Field()))
			case "email":
				data = append(data, fmt.Sprintf("%s is not valid email", fieldErr.Field()))
			case "gte":
				data = append(data, fmt.Sprintf("%s value must be greater than %s", fieldErr.Field(), fieldErr.Param()))
			case "lte":
				data = append(data, fmt.Sprintf("%s value must be lower than %s", fieldErr.Field(), fieldErr.Param()))
			default:
				data = append(data, "")
			}
		}

		return strings.Join(data, ", ")
	}
	return err.Error()
}
